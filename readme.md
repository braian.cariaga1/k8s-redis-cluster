# Redis Cluster

## TL;DR

GO TO: [Overriding with your config](#overriding-with-your-config)

---

## Use

1. Include this repo to you `kustomization.yml`
2. REPLACE names to match with your service.

# MANDATORY REPLACEMENTS:

| File              | Field                                                                                              | Value                                        | Description                                                         |
| ----------------- | -------------------------------------------------------------------------------------------------- | -------------------------------------------- | ------------------------------------------------------------------- |
| `*.yml`           | `metadata.name`                                                                                    | name of the redis cluster                    | use this param to identify your redis-cluster                       |
| `job.yml`         | `spec.template.spec.containers.[name=redis-cluster-starter].env.[name=INSTANCE_NAME].value`        | name of app (from statefull `metadata.name`) | name of redis instance, used for create hosts url to create cluster |
| `job.yml`         | `spec.template.spec.initContainers.[name=redis-cluster-starter].env.[name=SERVICE_NAME].value`     | headless service name                        | headless service to create redis instance url                       |
| `job.yml`         | `spec.template.spec.initContainers.[name=wait-for-redis-instances].env.[name=INSTANCE_NAME].value` | name of app (from statefull `metadata.name`) | name of redis instance, used for create hosts url to create cluster |
| `job.yml`         | `spec.template.spec.containers.[name=wait-for-redis-instances].env.[name=SERVICE_NAME].value`      | headless service name                        | headless service to create redis instance url                       |
| `service*.yml`    | `spec.selector.app`                                                                                | StatefulSet Name                             | replace this value to target the services to redis cluster pod's    |
| `statefulSet.yml` | `spec.selector.matchLabels.app`                                                                    | StatefulSet Name                             | replace this value to match with the services selector              |
| `statefulSet.yml` | `spec.template.metadata.labels.app`                                                                | StatefulSet Name                             | replace this value to match with the services selector              |

---

## Examples

`kustomization.yml`:

```yml
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization

resources:
  - https://gitlab.com/braian.cariaga1/k8s-redis-cluster.git
```

---

## Overriding with your config

`kustomization.yml`:

```yml
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization

resources:
  - https://gitlab.com/braian.cariaga1/k8s-redis-cluster.git

namePrefix: REPLACE_ME_WITH_YOUR_APP_NAME-

replacements:
  - source:
      kind: Service
      name: REPLACE_ME_WITH_YOUR_APP_NAME-redis-cluster-service-headless
      fieldPath: metadata.name
    targets:
      - select:
          kind: Job
          name: REPLACE_ME_WITH_YOUR_APP_NAME-redis-cluster-starter
        fieldPaths:
          - spec.template.spec.containers.[name=redis-cluster-starter].env.[name=SERVICE_NAME].value
          - spec.template.spec.initContainers.[name=wait-for-redis-instances].env.[name=SERVICE_NAME].value
  - source:
      kind: StatefulSet
      name: REPLACE_ME_WITH_YOUR_APP_NAME-redis-cluster
      fieldPath: metadata.name
    targets:
      - select:
          kind: Job
          name: REPLACE_ME_WITH_YOUR_APP_NAME-redis-cluster-starter
        fieldPaths:
          - spec.template.spec.containers.[name=redis-cluster-starter].env.[name=INSTANCE_NAME].value
          - spec.template.spec.initContainers.[name=wait-for-redis-instances].env.[name=INSTANCE_NAME].value
      - select:
          kind: Service
        fieldPaths:
          - spec.selector.app
      - select:
          kind: StatefulSet
          name: REPLACE_ME_WITH_YOUR_APP_NAME-redis-cluster
        fieldPaths:
          - spec.selector.matchLabels.app
          - spec.template.metadata.labels.app
```
